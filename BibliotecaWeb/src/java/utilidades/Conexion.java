package utilidades;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    private static Connection con = null;
    
    public static Connection getConexion(){
        try{
            if(con == null){
                String driver = "com.mysql.jdbc.Driver";
                String url = "jdbc:mysql://localhost/biblioteca";
                String user = "root";
                String password = "";
                Class.forName(driver);
                con = DriverManager.getConnection(url, user, password);
            }
            
        }catch(ClassNotFoundException | SQLException ex){
            System.out.println(ex.getMessage());
        }
        return con;
    }

}
