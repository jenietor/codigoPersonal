package utilidades;

import java.sql.Connection;
import modeloDao.SocioDao;
import modeloDto.SocioDto;

public class Main {

    public static void main(String[] args) {

        Connection prueba = Conexion.getConexion();
        if(prueba != null){
            System.out.println("Ok");
        }else{
            System.out.println("ERROR");
        }
        SocioDto nuevoSocioDto = new SocioDto(12345678, "Kennit ", "Romero ");
        SocioDao nuevoSocioDao = new SocioDao();
        //nuevoSocioDao.insertarNuevoSocio(nuevoSocioDto);
        System.out.println(nuevoSocioDao.leerSocio());
    }

}
