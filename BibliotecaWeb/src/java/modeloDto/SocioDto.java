/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDto;

/**
 *
 * @author Eduard
 */
public class SocioDto {

    private int dni = 0;
    private String nombres = null;
    private String apellidos = null;

    public SocioDto(int dni, String nombres, String apellidos) {
        this.dni = dni;
        this.nombres = nombres;
        this.apellidos = apellidos;
    }
    
    public SocioDto(){
        
    }

    //Setters
    public void setDni(int dni) {
        this.dni = dni;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    //Getters
    public int getDni() {
        return dni;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    //SobreEscribir el metodo toString
    public String toString() {
        return "Dni: " + this.dni
                + "\nNombres: " + this.nombres
                + "\nApellidos: " + this.apellidos;
    }
}
