/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modeloDto.SocioDto;
import utilidades.Conexion;

/**
 *
 * @author Eduard
 */
public class SocioDao {

    private Connection nuevaConexion = null;
    private PreparedStatement preparandoSentencia = null;
    private String sqlTemp = null;
    private String mensaje = "";
    private int resultado = 0;
    private ResultSet rsst = null;

    public SocioDao() {
        this.nuevaConexion = Conexion.getConexion();
    }

    public String insertarNuevoSocio(SocioDto nuevoSocioDto) {
        sqlTemp = "INSERT INTO `socios`(dni, `nombres`, `apellidos`)"
                + " VALUES (?, ?, ?)";
        try {

            preparandoSentencia = this.nuevaConexion.prepareStatement(sqlTemp);
            preparandoSentencia.setInt(1, nuevoSocioDto.getDni());
            preparandoSentencia.setString(2, nuevoSocioDto.getNombres());
            preparandoSentencia.setString(3, nuevoSocioDto.getApellidos());
            resultado = preparandoSentencia.executeUpdate();
            if (resultado != 0) {
                System.out.println("El registro se ha realizado satisfactoriamente");

            } else {
                System.out.println("Lo sentimos pero no se ha podido realizar el registro");

            }
        } catch (SQLException exc) {
            System.out.println(exc.getMessage());
        }
        return mensaje;
    }

    public ArrayList<SocioDto> leerSocio() {
        sqlTemp = "SELECT `dni`, `nombres`, `apellidos`  FROM `socios`";
        ArrayList<SocioDto> socios = new ArrayList();
        SocioDto temporal;

        try {
            preparandoSentencia = nuevaConexion.prepareStatement(sqlTemp);
            rsst = preparandoSentencia.executeQuery();
            while (rsst.next()) {
                temporal = new SocioDto();
                temporal.setDni(rsst.getInt("dni"));
                temporal.setNombres(rsst.getString("nombres"));
                temporal.setApellidos(rsst.getString("apellidos"));
                socios.add(temporal);
            }

        } catch (SQLException exc) {
            System.out.println(exc.getMessage());
        }
        return socios;
    }
}
